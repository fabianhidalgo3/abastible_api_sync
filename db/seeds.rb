

p "Creando Región y Provincias de Región de Valparaiso" # V
Region.create(id:5, codigo: '05',nombre: 'V')
p "Creando Región y Provincias de Región Metropolitana de Santiago"
Region.create(id:13, codigo: '13',nombre: 'XIII')
p "Creando Región y Provincias de Región de los Lagos"
Region.create(id:10, codigo: '10',nombre: 'X')

puts "Creando zonas Los Lagos"
Zona.create(id: 1, codigo: "'LOS", nombre: "Los Lagos", region_id: 10)
Zona.create(id: 2,codigo: "'OPM", nombre: "Puerto Montt", region_id: 10)
  
 puts "Creando tipos de lecturas"
 TipoLectura.create(id: 1,nombre:'Normal')
 TipoLectura.create(id: 2,nombre:'Verificación')

 puts "Creando empresa"
 Empresa.create(rut:'11.111.111-1', razon_social: 'Abastible', giro:'Servicios Luz', direccion:'')


  p "Creando marcas"
  Marca.create(nombre: 'Motorola')
  Marca.create(nombre: 'Hawei')
  Marca.create(nombre: 'Intermec')
  Marca.create(nombre: 'Samsung')


 ############################
 #       Modelo     	       #
 ############################
 puts "Creando modelos"
 Modelo.create(nombre: 'Moto Maxx', marca_id: 1)
 Modelo.create(nombre: 'Moto G 3', marca_id: 1)
 Modelo.create(nombre: 'Moto G 4', marca_id: 1)
 Modelo.create(nombre: 'Moto X', marca_id: 1)
 Modelo.create(nombre: 'Hawei P9 Lite', marca_id: 2)
 Modelo.create(nombre: 'Hawei P8 Lite', marca_id: 2)
 Modelo.create(nombre: 'Hawei P8', marca_id: 2)
 Modelo.create(nombre: 'Hawei P9', marca_id: 2)
 Modelo.create(nombre: 'Hawei P10', marca_id: 2)
 Modelo.create(nombre: 'Hawei MATE 9', marca_id: 2)
 Modelo.create(nombre: 'Hawei Y6 II', marca_id: 2)
 Modelo.create(nombre: 'Intermec CN51', marca_id: 3)
 Modelo.create(nombre: 'Intermec CN50', marca_id: 3)
 Modelo.create(nombre: 'Intermec CS40', marca_id: 3)
 Modelo.create(nombre: 'Intermec CK70', marca_id: 3)
 Modelo.create(nombre: 'Samsung J1', marca_id: 4)
 Modelo.create(nombre: 'Samsung J2', marca_id: 4)
 Modelo.create(nombre: 'Samsung J5', marca_id: 4)
 Modelo.create(nombre: 'Samsung J7', marca_id: 4)
 Modelo.create(nombre: 'Samsung J7 Prime', marca_id: 4)

 
 ############################
 #       Empresa      	   #
 ############################
 puts "Creando empresa"

 Empresa.create(rut:'11111111', razon_social: 'Abastible', giro:'Servicios de Gas', direccion:'')

 ############################
 #       EstadoLectura      #
 ############################
 puts "Creando estados de lectura"

 EstadoLectura.create(nombre:'Sin Asignar')
 EstadoLectura.create(nombre:'Asignado')
 EstadoLectura.create(nombre:'En Capturador')
 EstadoLectura.create(nombre:'Leido')
 EstadoLectura.create(nombre:'Enviado a ERP')

 ClaveLectura.create(codigo:1,nombre:'NADIE EN CASA')
ClaveLectura.create(codigo:10,nombre:'LECTURA CON DIFICULTAD')
ClaveLectura.create(codigo:11,nombre:'Lectura sin dificultad')
ClaveLectura.create(codigo:12,nombre:'Olor a gas en nicho')
ClaveLectura.create(codigo:13,nombre:'Olor a gas en tanque')
ClaveLectura.create(codigo:14,nombre:'Medidor daño A')
ClaveLectura.create(codigo:15,nombre:'Medidor daño B')
ClaveLectura.create(codigo:16,nombre:'Medidor daño C')
ClaveLectura.create(codigo:18,nombre:'Reja candado ajeno')
ClaveLectura.create(codigo:19,nombre:'Indicador % defectuoso')
ClaveLectura.create(codigo:2,nombre:'Conserje inubicable')
ClaveLectura.create(codigo:20,nombre:'PENDIENTE POR DISTANCIA')
ClaveLectura.create(codigo:21,nombre:'PUERTAS TRABADAS NO ABREN')
ClaveLectura.create(codigo:22,nombre:'UT no identificada')
ClaveLectura.create(codigo:3,nombre:'Nicho candado ajeno')
ClaveLectura.create(codigo:34,nombre:'Medidor N° empañado')
ClaveLectura.create(codigo:35,nombre:'Medidor N° ilegibles')
ClaveLectura.create(codigo:38,nombre:'Cliente gas natural')
ClaveLectura.create(codigo:4,nombre:'REJA PERIM CERRADA')
ClaveLectura.create(codigo:41,nombre:'Instrum bajo agua')
ClaveLectura.create(codigo:44,nombre:'CLIENTE NO DEJA LEER')
ClaveLectura.create(codigo:45,nombre:'Conex fraudulenta')
ClaveLectura.create(codigo:47,nombre:'NADIE EN CASA SOLO NIÑOS')
ClaveLectura.create(codigo:49,nombre:'Zona de asalto')
ClaveLectura.create(codigo:5,nombre:'CAMINO INTRANSITABLE')
ClaveLectura.create(codigo:50,nombre:'CANDADO TANQUE AGRIPADO')
ClaveLectura.create(codigo:6,nombre:'ANIMAL NO PERMITE LEER')
ClaveLectura.create(codigo:60,nombre:'DESHABITADO')
ClaveLectura.create(codigo:61,nombre:'EN VENTA')
ClaveLectura.create(codigo:62,nombre:'EN ARRIENDO')
ClaveLectura.create(codigo:63,nombre:'GAS CORTADO')
ClaveLectura.create(codigo:64,nombre:'POSIBLE ATASCADO')
ClaveLectura.create(codigo:7,nombre:'CLIENTE SIN MEDIDOR')
ClaveLectura.create(codigo:71,nombre:'Tapa con candado ajeno')
ClaveLectura.create(codigo:72,nombre:'TAPA AGRIPADA')
ClaveLectura.create(codigo:73,nombre:'EN RECINTO PRIVADO')
ClaveLectura.create(codigo:8,nombre:'VEHICULO EN NICHO')
ClaveLectura.create(codigo:80,nombre:'NICHO CANDADO PART')
ClaveLectura.create(codigo:83,nombre:'Tanque nuevo')
ClaveLectura.create(codigo:84,nombre:'Lectura no realizada')
ClaveLectura.create(codigo:85,nombre:'Medidor con desperf')
ClaveLectura.create(codigo:86,nombre:'Lectura Conforme')
ClaveLectura.create(codigo:87,nombre:'Lectura telefónica')
ClaveLectura.create(codigo:88,nombre:'Validada por administrativo')
ClaveLectura.create(codigo:89,nombre:'Problema al leer')
ClaveLectura.create(codigo:9,nombre:'Lectura dada por cliente')
ClaveLectura.create(codigo:90,nombre:'Medidor distinto especificar')
ClaveLectura.create(codigo:91,nombre:'Volver a tomar lectura')
ClaveLectura.create(codigo:92,nombre:'Medidor no figura en palm')
ClaveLectura.create(codigo:93,nombre:'Fuga de gas especificar')
ClaveLectura.create(codigo:99,nombre:'Cierre de Periodo')
 p "Creando contratistas"
 Contratistum.create(id:1, nombre:'Provider')

 p "Creando perfiles de usuario"
 Perfil.create(id: 1, nombre: 'GOD')
 Perfil.create(id: 2, nombre: 'Administrador de Contrato')
 Perfil.create(id: 3, nombre: 'Coordinador')
 Perfil.create(id: 4, nombre: 'Analista')
 Perfil.create(id: 5, nombre: 'Operador de Medidor')

 p "Creando usuarios Admin del Sistema"
 Usuario.create(id: 1, email: 'deploy', password: 'P@ssw0rd666', perfil_id: 1)
 Usuario.create(id: 2, email: 'jose', password: 'josecorail', perfil_id: 2)
 
 p "Creando empleados Admin del Sistema"
 Empleado.create(id: 1, nombre:'Fabián Andrés', apellido_paterno:'Hidalgo', apellido_materno: 'Neira', rut:'18.681.243-3', contratista_id: 1,empresa_id:1, usuario_id:1)
 Empleado.create(id: 2, nombre:'José Fernando', apellido_paterno:'Corail', apellido_materno: 'Moreno', rut:'11.620.739-7', contratista_id: 1,empresa_id:1, usuario_id:2)
 
 
 
 
 puts "Creo Admin Plan Piloto"
 Usuario.create(id: 3,  email: 'abastible', password:'abastible', perfil_id: 2)
 Empleado.create(id: 3, nombre:'abastible', rut:'0-1', contratista_id: 1, empresa_id:1, usuario_id:3)


empleados = Empleado.all
zonas = Zona.all

empleados.each do |empleado|
  zonas.each do |zona|
    EmpleadosZonas.create(empleado_id: empleado.id, zona_id: zona.id)
  end
end



p "Carga de Datos para Reparto"
p "Creo estado de Repartos"
EstadoReparto.create(nombre: "Sin Asignar")
EstadoReparto.create(nombre: "Asignado")
EstadoReparto.create(nombre: "Cargado")
EstadoReparto.create(nombre: "Repartido")
EstadoReparto.create(nombre: "Proceso Cerrado")
p "Creo Tipo Documentos"
TipoDocumento.create(nombre: "Boleta")
TipoDocumento.create(nombre: "Factura")

