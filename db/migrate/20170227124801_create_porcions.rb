class CreatePorcions < ActiveRecord::Migration[5.0]
  def change
    create_table :porcions do |t|
      t.string :codigo
      t.string :nombre
      t.integer :mes
      t.integer :ano
      t.boolean :abierto
      t.references :zona, foreign_key: true
      t.timestamps
    end
  end
end
