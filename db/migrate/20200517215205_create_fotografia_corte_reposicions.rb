class CreateFotografiaCorteReposicions < ActiveRecord::Migration[5.0]
  def change
    create_table :fotografia_corte_reposicions do |t|
      t.references :orden_corte_reposicion, foreign_key: true
      t.string :archivo
      t.string :descripcion
      t.timestamps
    end
  end
end
