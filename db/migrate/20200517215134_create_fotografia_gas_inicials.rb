class CreateFotografiaGasInicials < ActiveRecord::Migration[5.0]
  def change
    create_table :fotografia_gas_inicials do |t|
      t.references :orden_gas_inicial, foreign_key: true
      t.string :archivo
      t.string :descripcion
      t.timestamps
    end
  end
end
