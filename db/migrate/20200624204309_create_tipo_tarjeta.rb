class CreateTipoTarjeta < ActiveRecord::Migration[5.0]
  def change
    create_table :tipo_tarjeta do |t|
      t.string :codigo
      t.string :descripcion
      t.timestamps
    end
  end
end
