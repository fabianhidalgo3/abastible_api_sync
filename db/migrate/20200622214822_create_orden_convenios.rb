class CreateOrdenConvenios < ActiveRecord::Migration[5.0]
  def change
    create_table :orden_convenios do |t|
      t.string :codigo
      t.integer :secuencia
      t.string :direccion
      t.string :calle
      t.integer :numero
      t.datetime :fecha_carga
      t.datetime :fecha_propuesta
      t.datetime :fecha_asignacion
      t.datetime :fecha_visita
      t.datetime :fecha_reagendada
      t.decimal :gps_latitud, :precision => 10, :scale => 7
      t.decimal :gps_longitud, :precision => 10, :scale => 7
      t.boolean :verificacion # Lectura enviada a verificación
      t.integer :modulo_analisis # Modulo analisis donde salio a verificación
      t.boolean :aprobado #orden Aprobada por análista
      t.string :observacion_analista #Observación de Aprovación o Reprobación del Análista
      t.integer :color
      t.decimal :monto_deuda
      t.integer :morosidad
      t.decimal :abono
      t.integer :numero_cuotas
      t.decimal :valor_couta
      t.string :rut 
      t.string :nombre 
      t.string :correo_electronico
      t.string :telefono
      t.string :observacion_gestor
      t.references :medidor, foreign_key: true
      t.references :instalacion, foreign_key: true
      t.references :cliente, foreign_key: true
      t.references :ruta_convenio, foreign_key: true
      t.references :tipo_convenio, foreign_key: true
      t.references :proceso_convenio, foreign_key: true
      t.references :estado_convenio, foreign_key: true
      t.references :comuna, foreign_key: true
      t.references :clave_convenio, foreign_key: true
      t.timestamps
    end
  end
end
