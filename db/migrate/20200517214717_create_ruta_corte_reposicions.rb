class CreateRutaCorteReposicions < ActiveRecord::Migration[5.0]
  def change
    create_table :ruta_corte_reposicions do |t|
        t.string :codigo
        t.string :nombre
        t.integer :mes
        t.integer :ano
        t.boolean :abierto
        t.references :porcion_corte_reposicion, foreign_key: true
      t.timestamps
    end
  end
end
