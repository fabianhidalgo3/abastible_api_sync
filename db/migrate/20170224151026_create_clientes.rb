class CreateClientes < ActiveRecord::Migration[5.0]
  def change
    create_table :clientes do |t|
      t.integer :numero_cliente
      t.string :rut
      t.string :nombre
      t.string :apellido_paterno
      t.string :apellido_materno
      t.string :calle
      t.string :numero_domicilio
      t.string :direccion_completa
      t.decimal :gps_latitud, :precision => 10, :scale => 7
      t.decimal :gps_longitud, :precision => 10, :scale => 7
      t.string :observacion_domicilio
      t.timestamps
    end
  end
end
