class CreateEstadoCorteReposicions < ActiveRecord::Migration[5.0]
  def change
    create_table :estado_corte_reposicions do |t|
      t.string :nombre
      t.timestamps
    end
  end
end
