class AddTipoTarjetaToOrdenConvenio < ActiveRecord::Migration[5.0]
  def change
    add_reference :orden_convenios, :tipo_tarjeta, foreign_key: true
  end
end
