class CreateTipoConvenios < ActiveRecord::Migration[5.0]
  def change
    create_table :tipo_convenios do |t|
      t.string :codigo
      t.string :nombre
      t.timestamps
    end
  end
end
