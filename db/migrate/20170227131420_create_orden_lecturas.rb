class CreateOrdenLecturas < ActiveRecord::Migration[5.0]
  def change
    create_table :orden_lecturas do |t|
      t.string :codigo
      t.integer :secuencia_lector
      t.string :direccion
      t.string :direccion_entrega
      t.string :numero_poste
      t.string :observacion_lector
      t.datetime :fecha_carga
      t.datetime :fecha_propuesta
      t.datetime :fecha_asignacion
      t.decimal :gps_latitud, :precision => 10, :scale => 7
      t.decimal :gps_longitud, :precision => 10, :scale => 7
      t.decimal :ajuste_sencillo_anterior, :precision => 6, :scale => 2
      t.decimal :ajuste_sencillo_actual, :precision => 6, :scale => 2
      t.boolean :verificacion # Lectura enviada a verificación
      t.integer :modulo_analisis # Modulo analisis donde salio a verificación
      t.boolean :aprobado #orden Aprobada por análista
      t.string :observacion_analista #Observación de Aprovación o Reprobación del Análista
      t.integer :color
      t.references :medidor, foreign_key: true
      t.references :instalacion, foreign_key: true
      t.references :cliente, foreign_key: true
      t.references :rutum, foreign_key: true
      t.references :tipo_lectura, foreign_key: true
      t.references :estado_lectura, foreign_key: true
      t.references :tarifa, foreign_key: true
      t.references :comuna, foreign_key: true
      t.references :contratista, foreign_key: true
      t.references :carga_sistema, foreign_key: true
      t.timestamps
    end
  end
end
