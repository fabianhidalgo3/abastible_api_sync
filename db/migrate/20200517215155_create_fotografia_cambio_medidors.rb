class CreateFotografiaCambioMedidors < ActiveRecord::Migration[5.0]
  def change
    create_table :fotografia_cambio_medidors do |t|
      t.references :orden_cambio_medidor, foreign_key: true
      t.string :archivo
      t.string :descripcion
      t.timestamps
    end
  end
end
