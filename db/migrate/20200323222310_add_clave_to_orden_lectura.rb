class AddClaveToOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_reference :orden_lecturas, :clave_lectura, foreign_key: true
  end
end
