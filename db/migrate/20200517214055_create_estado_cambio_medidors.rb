class CreateEstadoCambioMedidors < ActiveRecord::Migration[5.0]
  def change
    create_table :estado_cambio_medidors do |t|
      t.string :nombre
      t.timestamps
    end
  end
end
