class AddIndiceToRuta < ActiveRecord::Migration[5.0]
  def change
    add_index :ruta, :codigo
    add_index :ruta, :nombre
  end
end
