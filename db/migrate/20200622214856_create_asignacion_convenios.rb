class CreateAsignacionConvenios < ActiveRecord::Migration[5.0]
  def change
    create_table :asignacion_convenios do |t|
      t.references :ruta_convenio, foreign_key: true
      t.references :orden_convenio, foreign_key: true
      t.references :empleado, foreign_key: true
      t.timestamps
    end
  end
end
