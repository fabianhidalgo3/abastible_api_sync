class AddDespachoPostalToCliente < ActiveRecord::Migration[5.0]
  def change
    add_column :clientes, :despacho_postal, :boolean
    add_column :clientes, :factura_electronica, :boolean
  end
end
