class CreateOrdenRepartos < ActiveRecord::Migration[5.0]
  def change
    create_table :orden_repartos do |t|
      t.string :codigo
      t.string :domicilio
      t.datetime :fecha_carga
      t.datetime :fecha_asignacion
      t.datetime :fecha_ejecucion
      t.decimal :gps_latitud ,:precision => 10, :scale => 7
      t.decimal :gps_longitud, :precision => 10, :scale => 7
      t.integer :secuencia_lector
      t.references :estado_reparto, foreign_key: true
      t.references :tipo_documento, foreign_key: true
      t.references :cliente, foreign_key: true
      t.references :medidor, foreign_key: true
      t.references :instalacion, foreign_key: true
      t.string :direccion_entrega
      t.references :ruta_reparto, foreign_key: true
      t.references :comuna, foreign_key: true
      t.timestamps
    end
  end
end
