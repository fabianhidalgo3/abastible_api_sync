class CreateFotografiaConvenios < ActiveRecord::Migration[5.0]
  def change
    create_table :fotografia_convenios do |t|
      t.references :orden_convenio, foreign_key: true
      t.string :archivo
      t.string :descripcion
      t.timestamps
    end
  end
end
