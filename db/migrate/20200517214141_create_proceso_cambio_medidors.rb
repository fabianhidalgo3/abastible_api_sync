class CreateProcesoCambioMedidors < ActiveRecord::Migration[5.0]
  def change
    create_table :proceso_cambio_medidors do |t|
      t.string :nombre
      t.timestamps
    end
  end
end
