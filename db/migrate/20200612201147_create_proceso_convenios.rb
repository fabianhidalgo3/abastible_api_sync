class CreateProcesoConvenios < ActiveRecord::Migration[5.0]
  def change
    create_table :proceso_convenios do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
