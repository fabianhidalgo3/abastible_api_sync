class CreateAsignacionCorteReposicions < ActiveRecord::Migration[5.0]
  def change
    create_table :asignacion_corte_reposicions do |t|
      t.references :ruta_corte_reposicion, foreign_key: true
      t.references :orden_corte_reposicion, foreign_key: true
      t.references :empleado, foreign_key: true
      t.timestamps
    end
  end
end
