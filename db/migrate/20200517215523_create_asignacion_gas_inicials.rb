class CreateAsignacionGasInicials < ActiveRecord::Migration[5.0]
  def change
    create_table :asignacion_gas_inicials do |t|
      t.references :ruta_gas_inicial, foreign_key: true
      t.references :orden_gas_inicial, foreign_key: true
      t.references :empleado, foreign_key: true
      t.timestamps
    end
  end
end
