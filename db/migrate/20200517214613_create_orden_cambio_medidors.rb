class CreateOrdenCambioMedidors < ActiveRecord::Migration[5.0]
  def change
    create_table :orden_cambio_medidors do |t|
      t.string :codigo
      t.integer :secuencia
      t.string :direccion
      t.string :calle
      t.integer :numero
      t.integer :modulo_analisis # Modulo analisis donde salio a verificación
      t.integer :color
      t.datetime :fecha_carga
      t.datetime :fecha_propuesta
      t.datetime :fecha_asignacion
      t.decimal :gps_latitud, :precision => 10, :scale => 7
      t.decimal :gps_longitud, :precision => 10, :scale => 7
      t.boolean :verificacion # Lectura enviada a verificación
      t.boolean :aprobado #orden Aprobada por análista
      t.string :observacion_analista #Observación de Aprovación o Reprobación del Análista
      t.string :lectura_encontrada
      t.string :numero_nuevo_medidor
      t.string :lectura_nuevo_medidor
      t.string :nombre_cliente
      t.string :rut_ciente
      t.string :telefono_cliente
      t.string :correo_cliente
      t.string :observacion_tecnico
      t.datetime :fecha_ejecucion
      t.references :medidor, foreign_key: true
      t.references :instalacion, foreign_key: true
      t.references :cliente, foreign_key: true
      t.references :ruta_cambio_medidor, foreign_key: true
      t.references :proceso_cambio_medidor, foreign_key: true
      t.references :estado_cambio_medidor, foreign_key: true
      t.references :clave_cambio_medidore, foreign_key: true
      t.references :comuna, foreign_key: true
      t.timestamps
    end
  end
end
