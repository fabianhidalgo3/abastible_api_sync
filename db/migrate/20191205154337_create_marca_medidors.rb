class CreateMarcaMedidors < ActiveRecord::Migration[5.0]
  def change
    create_table :marca_medidors do |t|
      t.string :codigo
      t.string :nombre

      t.timestamps
    end
  end
end
