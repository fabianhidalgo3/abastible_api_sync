class AddToLectuerasToOrdenLectura < ActiveRecord::Migration[5.0]
  def change
    add_column :orden_lecturas, :lectura_anterior, :decimal
    add_column :orden_lecturas, :lectura_actual, :decimal
    add_column :orden_lecturas, :lectura_modificada, :decimal
    add_column :orden_lecturas, :lectura_dictada, :decimal
    add_column :orden_lecturas, :fecha_ejecucion, :datetime
    add_column :orden_lecturas, :consumo_minimo, :decimal
    add_column :orden_lecturas, :consumo_maximo, :decimal
    add_column :orden_lecturas, :consumo_promedio, :decimal
    add_column :orden_lecturas, :consumo_actual, :decimal
  end
end
