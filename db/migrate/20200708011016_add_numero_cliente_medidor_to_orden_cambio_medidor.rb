class AddNumeroClienteMedidorToOrdenCambioMedidor < ActiveRecord::Migration[5.0]
  def change
    add_column :orden_cambio_medidors, :numero_cliente, :string
    add_column :orden_cambio_medidors, :numero_medidor, :string
  end
end
