class AddRangosToTipoConvenio < ActiveRecord::Migration[5.0]
  def change
    add_column :tipo_convenios, :rango_a, :integer
    add_column :tipo_convenios, :rango_b, :integer
  end
end
