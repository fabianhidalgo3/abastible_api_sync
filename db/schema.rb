# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20201007214202) do

  create_table "asignacion_cambio_medidors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "ruta_cambio_medidor_id"
    t.integer  "orden_cambio_medidor_id"
    t.integer  "empleado_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["empleado_id"], name: "index_asignacion_cambio_medidors_on_empleado_id", using: :btree
    t.index ["orden_cambio_medidor_id"], name: "index_asignacion_cambio_medidors_on_orden_cambio_medidor_id", using: :btree
    t.index ["ruta_cambio_medidor_id"], name: "index_asignacion_cambio_medidors_on_ruta_cambio_medidor_id", using: :btree
  end

  create_table "asignacion_convenios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "ruta_convenio_id"
    t.integer  "orden_convenio_id"
    t.integer  "empleado_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["empleado_id"], name: "index_asignacion_convenios_on_empleado_id", using: :btree
    t.index ["orden_convenio_id"], name: "index_asignacion_convenios_on_orden_convenio_id", using: :btree
    t.index ["ruta_convenio_id"], name: "index_asignacion_convenios_on_ruta_convenio_id", using: :btree
  end

  create_table "asignacion_corte_reposicions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "ruta_corte_reposicion_id"
    t.integer  "orden_corte_reposicion_id"
    t.integer  "empleado_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["empleado_id"], name: "index_asignacion_corte_reposicions_on_empleado_id", using: :btree
    t.index ["orden_corte_reposicion_id"], name: "index_asignacion_corte_reposicions_on_orden_corte_reposicion_id", using: :btree
    t.index ["ruta_corte_reposicion_id"], name: "index_asignacion_corte_reposicions_on_ruta_corte_reposicion_id", using: :btree
  end

  create_table "asignacion_gas_inicials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "ruta_gas_inicial_id"
    t.integer  "orden_gas_inicial_id"
    t.integer  "empleado_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["empleado_id"], name: "index_asignacion_gas_inicials_on_empleado_id", using: :btree
    t.index ["orden_gas_inicial_id"], name: "index_asignacion_gas_inicials_on_orden_gas_inicial_id", using: :btree
    t.index ["ruta_gas_inicial_id"], name: "index_asignacion_gas_inicials_on_ruta_gas_inicial_id", using: :btree
  end

  create_table "asignacion_lecturas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "rutum_id"
    t.integer  "orden_lectura_id"
    t.integer  "empleado_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["empleado_id"], name: "index_asignacion_lecturas_on_empleado_id", using: :btree
    t.index ["orden_lectura_id"], name: "index_asignacion_lecturas_on_orden_lectura_id", using: :btree
    t.index ["rutum_id"], name: "index_asignacion_lecturas_on_rutum_id", using: :btree
  end

  create_table "asignacion_modificacion_lecturas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "empleado_id"
    t.integer  "orden_lectura_id"
    t.decimal  "lectura_actual",     precision: 10
    t.integer  "clave_lectura_id"
    t.datetime "fecha_modificacion"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "rutum_id"
    t.index ["clave_lectura_id"], name: "index_asignacion_modificacion_lecturas_on_clave_lectura_id", using: :btree
    t.index ["empleado_id"], name: "index_asignacion_modificacion_lecturas_on_empleado_id", using: :btree
    t.index ["orden_lectura_id"], name: "index_asignacion_modificacion_lecturas_on_orden_lectura_id", using: :btree
    t.index ["rutum_id"], name: "index_asignacion_modificacion_lecturas_on_rutum_id", using: :btree
  end

  create_table "asignacion_repartos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "ruta_reparto_id"
    t.integer  "empleado_id"
    t.integer  "orden_reparto_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["empleado_id"], name: "index_asignacion_repartos_on_empleado_id", using: :btree
    t.index ["orden_reparto_id"], name: "index_asignacion_repartos_on_orden_reparto_id", using: :btree
    t.index ["ruta_reparto_id"], name: "index_asignacion_repartos_on_ruta_reparto_id", using: :btree
  end

  create_table "carga_sistemas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.integer  "mes"
    t.integer  "ano"
    t.integer  "porcion"
    t.datetime "fecha_carga"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "clave_cambio_medidores", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.boolean  "requerido"
    t.boolean  "efectivo"
    t.boolean  "activo"
    t.integer  "numero_fotografias"
    t.integer  "secuencia"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "clave_convenios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "nombre"
    t.integer  "secuencia"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "activo"
    t.boolean  "efectivo"
    t.boolean  "requerido"
    t.integer  "numero_fotorafias"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "clave_corte_reposicions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.boolean  "requerido"
    t.boolean  "efectivo"
    t.boolean  "activo"
    t.integer  "numero_fotografias"
    t.integer  "secuencia"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "clave_gas_inicials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.boolean  "requerido"
    t.boolean  "efectivo"
    t.boolean  "activo"
    t.integer  "numero_fotografias"
    t.integer  "secuencia"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "clave_lecturas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.boolean  "requerido"
    t.boolean  "efectivo"
    t.boolean  "factura"
    t.integer  "numero_fotografias"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "secuencia"
    t.boolean  "activo"
  end

  create_table "clientes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "numero_cliente"
    t.string   "rut"
    t.string   "nombre"
    t.string   "apellido_paterno"
    t.string   "apellido_materno"
    t.string   "calle"
    t.string   "numero_domicilio"
    t.string   "direccion_completa"
    t.decimal  "gps_latitud",           precision: 10, scale: 7
    t.decimal  "gps_longitud",          precision: 10, scale: 7
    t.string   "observacion_domicilio"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "empresa_id"
    t.boolean  "despacho_postal"
    t.boolean  "factura_electronica"
    t.integer  "secuencia"
    t.index ["empresa_id"], name: "index_clientes_on_empresa_id", using: :btree
    t.index ["numero_cliente"], name: "index_clientes_on_numero_cliente", using: :btree
  end

  create_table "comunas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "codigo_sii"
    t.string   "codigo_tesoreria"
    t.string   "nombre"
    t.integer  "provincium_id"
    t.integer  "zona_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["provincium_id"], name: "index_comunas_on_provincium_id", using: :btree
    t.index ["zona_id"], name: "index_comunas_on_zona_id", using: :btree
  end

  create_table "contratista", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "detalle_orden_lecturas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "orden_lectura_id"
    t.decimal  "lectura_anterior",       precision: 10
    t.decimal  "consumo_promedio",       precision: 10
    t.decimal  "consumo_maximo",         precision: 10
    t.decimal  "consumo_minimo",         precision: 10
    t.decimal  "lectura_actual",         precision: 10
    t.decimal  "lectura_dictada",        precision: 10
    t.datetime "fecha_ejecucion"
    t.string   "folio_casa_cerrada"
    t.integer  "clave_lectura_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.datetime "fecha_lectura_anterior"
    t.integer  "consumo_actual"
    t.index ["clave_lectura_id"], name: "index_detalle_orden_lecturas_on_clave_lectura_id", using: :btree
    t.index ["orden_lectura_id"], name: "index_detalle_orden_lecturas_on_orden_lectura_id", using: :btree
  end

  create_table "empleados", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.string   "apellido_paterno"
    t.string   "apellido_materno"
    t.string   "rut"
    t.integer  "contratista_id"
    t.integer  "empresa_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "usuario_id"
    t.string   "codigo"
    t.index ["contratista_id"], name: "index_empleados_on_contratista_id", using: :btree
    t.index ["empresa_id"], name: "index_empleados_on_empresa_id", using: :btree
    t.index ["usuario_id"], name: "index_empleados_on_usuario_id", using: :btree
  end

  create_table "empleados_zonas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "empleado_id"
    t.integer  "zona_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["empleado_id"], name: "index_empleados_zonas_on_empleado_id", using: :btree
    t.index ["zona_id"], name: "index_empleados_zonas_on_zona_id", using: :btree
  end

  create_table "empresas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "rut"
    t.string   "razon_social"
    t.string   "giro"
    t.string   "direccion"
    t.integer  "comuna_id"
    t.string   "imagen"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["comuna_id"], name: "index_empresas_on_comuna_id", using: :btree
  end

  create_table "equipos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.string   "mac"
    t.integer  "modelo_id"
    t.integer  "empleado_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "quick_support"
    t.index ["empleado_id"], name: "index_equipos_on_empleado_id", using: :btree
    t.index ["modelo_id"], name: "index_equipos_on_modelo_id", using: :btree
  end

  create_table "estado_cambio_medidors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estado_convenios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estado_corte_reposicions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estado_gas_inicials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estado_lecturas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "estado_repartos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "fotografia", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "detalle_orden_lectura_id"
    t.string   "archivo"
    t.string   "descripcion"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "orden_lectura_id"
    t.index ["detalle_orden_lectura_id"], name: "index_fotografia_on_detalle_orden_lectura_id", using: :btree
    t.index ["orden_lectura_id"], name: "index_fotografia_on_orden_lectura_id", using: :btree
  end

  create_table "fotografia_cambio_medidors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "orden_cambio_medidor_id"
    t.string   "archivo"
    t.string   "descripcion"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["orden_cambio_medidor_id"], name: "index_fotografia_cambio_medidors_on_orden_cambio_medidor_id", using: :btree
  end

  create_table "fotografia_convenios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "orden_convenio_id"
    t.string   "archivo"
    t.string   "descripcion"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["orden_convenio_id"], name: "index_fotografia_convenios_on_orden_convenio_id", using: :btree
  end

  create_table "fotografia_corte_reposicions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "orden_corte_reposicion_id"
    t.string   "archivo"
    t.string   "descripcion"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["orden_corte_reposicion_id"], name: "index_fotografia_corte_reposicions_on_orden_corte_reposicion_id", using: :btree
  end

  create_table "fotografia_gas_inicials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "orden_gas_inicial_id"
    t.string   "archivo"
    t.string   "descripcion"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["orden_gas_inicial_id"], name: "index_fotografia_gas_inicials_on_orden_gas_inicial_id", using: :btree
  end

  create_table "instalacions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["codigo"], name: "index_instalacions_on_codigo", using: :btree
  end

  create_table "intentos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "detalle_orden_lectura_id"
    t.decimal  "lectura",                  precision: 10
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.index ["detalle_orden_lectura_id"], name: "index_intentos_on_detalle_orden_lectura_id", using: :btree
  end

  create_table "marca_medidors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "marcas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medidors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "numero_medidor"
    t.integer  "numero_digitos"
    t.integer  "diametro"
    t.integer  "ano"
    t.string   "ubicacion_medidor"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "modelo_medidor_id"
    t.index ["modelo_medidor_id"], name: "index_medidors_on_modelo_medidor_id", using: :btree
    t.index ["numero_medidor"], name: "index_medidors_on_numero_medidor", using: :btree
  end

  create_table "modelo_medidors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "marca_medidor_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["marca_medidor_id"], name: "index_modelo_medidors_on_marca_medidor_id", using: :btree
  end

  create_table "modelos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.text     "caracteristicas", limit: 65535
    t.integer  "marca_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["marca_id"], name: "index_modelos_on_marca_id", using: :btree
  end

  create_table "monitor_lectores", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer  "rutum_id"
    t.integer  "empleado_id"
    t.integer  "asignacion"
    t.integer  "cargado"
    t.integer  "pendiente_visita"
    t.integer  "total_efectivo"
    t.integer  "total_no_efectivo"
    t.integer  "leidos"
    t.string   "efectividad"
    t.datetime "fecha_inicio"
    t.datetime "fecha_termino"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["empleado_id"], name: "index_monitor_lectores_on_empleado_id", using: :btree
    t.index ["rutum_id"], name: "index_monitor_lectores_on_rutum_id", using: :btree
  end

  create_table "orden_cambio_medidors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.integer  "secuencia"
    t.string   "direccion"
    t.string   "calle"
    t.integer  "numero"
    t.integer  "modulo_analisis"
    t.integer  "color"
    t.datetime "fecha_carga"
    t.datetime "fecha_propuesta"
    t.datetime "fecha_asignacion"
    t.decimal  "gps_latitud",               precision: 10, scale: 7
    t.decimal  "gps_longitud",              precision: 10, scale: 7
    t.boolean  "verificacion"
    t.boolean  "aprobado"
    t.string   "observacion_analista"
    t.string   "lectura_encontrada"
    t.string   "numero_nuevo_medidor"
    t.string   "lectura_nuevo_medidor"
    t.string   "nombre_cliente"
    t.string   "rut_ciente"
    t.string   "telefono_cliente"
    t.string   "correo_cliente"
    t.string   "observacion_tecnico"
    t.datetime "fecha_ejecucion"
    t.integer  "medidor_id"
    t.integer  "instalacion_id"
    t.integer  "cliente_id"
    t.integer  "ruta_cambio_medidor_id"
    t.integer  "proceso_cambio_medidor_id"
    t.integer  "estado_cambio_medidor_id"
    t.integer  "clave_cambio_medidore_id"
    t.integer  "comuna_id"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "numero_cliente"
    t.string   "numero_medidor"
    t.index ["clave_cambio_medidore_id"], name: "index_orden_cambio_medidors_on_clave_cambio_medidore_id", using: :btree
    t.index ["cliente_id"], name: "index_orden_cambio_medidors_on_cliente_id", using: :btree
    t.index ["comuna_id"], name: "index_orden_cambio_medidors_on_comuna_id", using: :btree
    t.index ["estado_cambio_medidor_id"], name: "index_orden_cambio_medidors_on_estado_cambio_medidor_id", using: :btree
    t.index ["instalacion_id"], name: "index_orden_cambio_medidors_on_instalacion_id", using: :btree
    t.index ["medidor_id"], name: "index_orden_cambio_medidors_on_medidor_id", using: :btree
    t.index ["proceso_cambio_medidor_id"], name: "index_orden_cambio_medidors_on_proceso_cambio_medidor_id", using: :btree
    t.index ["ruta_cambio_medidor_id"], name: "index_orden_cambio_medidors_on_ruta_cambio_medidor_id", using: :btree
  end

  create_table "orden_convenios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.integer  "secuencia"
    t.string   "direccion"
    t.string   "calle"
    t.integer  "numero"
    t.datetime "fecha_carga"
    t.datetime "fecha_propuesta"
    t.datetime "fecha_asignacion"
    t.datetime "fecha_visita"
    t.datetime "fecha_reagendada"
    t.decimal  "gps_latitud",          precision: 10, scale: 7
    t.decimal  "gps_longitud",         precision: 10, scale: 7
    t.boolean  "verificacion"
    t.integer  "modulo_analisis"
    t.boolean  "aprobado"
    t.string   "observacion_analista"
    t.integer  "color"
    t.decimal  "monto_deuda",          precision: 10
    t.integer  "morosidad"
    t.decimal  "abono",                precision: 10
    t.integer  "numero_cuotas"
    t.decimal  "valor_couta",          precision: 10
    t.string   "rut"
    t.string   "nombre"
    t.string   "correo_electronico"
    t.string   "telefono"
    t.string   "observacion_gestor"
    t.integer  "medidor_id"
    t.integer  "instalacion_id"
    t.integer  "cliente_id"
    t.integer  "ruta_convenio_id"
    t.integer  "tipo_convenio_id"
    t.integer  "proceso_convenio_id"
    t.integer  "estado_convenio_id"
    t.integer  "comuna_id"
    t.integer  "clave_convenio_id"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.integer  "tipo_tarjeta_id"
    t.string   "numero_medidor"
    t.string   "numero_cliente"
    t.index ["clave_convenio_id"], name: "index_orden_convenios_on_clave_convenio_id", using: :btree
    t.index ["cliente_id"], name: "index_orden_convenios_on_cliente_id", using: :btree
    t.index ["comuna_id"], name: "index_orden_convenios_on_comuna_id", using: :btree
    t.index ["estado_convenio_id"], name: "index_orden_convenios_on_estado_convenio_id", using: :btree
    t.index ["instalacion_id"], name: "index_orden_convenios_on_instalacion_id", using: :btree
    t.index ["medidor_id"], name: "index_orden_convenios_on_medidor_id", using: :btree
    t.index ["proceso_convenio_id"], name: "index_orden_convenios_on_proceso_convenio_id", using: :btree
    t.index ["ruta_convenio_id"], name: "index_orden_convenios_on_ruta_convenio_id", using: :btree
    t.index ["tipo_convenio_id"], name: "index_orden_convenios_on_tipo_convenio_id", using: :btree
    t.index ["tipo_tarjeta_id"], name: "index_orden_convenios_on_tipo_tarjeta_id", using: :btree
  end

  create_table "orden_corte_reposicions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.integer  "secuencia"
    t.string   "direccion"
    t.string   "calle"
    t.integer  "numero"
    t.datetime "fecha_carga"
    t.datetime "fecha_propuesta"
    t.datetime "fecha_asignacion"
    t.decimal  "gps_latitud",                               precision: 10, scale: 7
    t.decimal  "gps_longitud",                              precision: 10, scale: 7
    t.boolean  "verificacion"
    t.integer  "modulo_analisis"
    t.boolean  "aprobado"
    t.string   "observacion_analista"
    t.decimal  "deuda_total",                               precision: 10
    t.integer  "color"
    t.string   "lectura_encontrada"
    t.string   "observacion_tecnico"
    t.datetime "fecha_ejecucion"
    t.integer  "medidor_id"
    t.integer  "instalacion_id"
    t.integer  "cliente_id"
    t.integer  "ruta_corte_reposicion_id"
    t.integer  "tipo_corte_reposicion_id"
    t.integer  "proceso_corte_reposicion_id"
    t.integer  "estado_corte_reposicion_id"
    t.integer  "comuna_id"
    t.integer  "clave_corte_reposicion_id"
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.integer  "tipo_corte"
    t.text     "numero_medidor",              limit: 65535
    t.text     "numero_cliente",              limit: 65535
    t.integer  "morosidad"
    t.index ["clave_corte_reposicion_id"], name: "index_orden_corte_reposicions_on_clave_corte_reposicion_id", using: :btree
    t.index ["cliente_id"], name: "index_orden_corte_reposicions_on_cliente_id", using: :btree
    t.index ["comuna_id"], name: "index_orden_corte_reposicions_on_comuna_id", using: :btree
    t.index ["estado_corte_reposicion_id"], name: "index_orden_corte_reposicions_on_estado_corte_reposicion_id", using: :btree
    t.index ["instalacion_id"], name: "index_orden_corte_reposicions_on_instalacion_id", using: :btree
    t.index ["medidor_id"], name: "index_orden_corte_reposicions_on_medidor_id", using: :btree
    t.index ["proceso_corte_reposicion_id"], name: "index_orden_corte_reposicions_on_proceso_corte_reposicion_id", using: :btree
    t.index ["ruta_corte_reposicion_id"], name: "index_orden_corte_reposicions_on_ruta_corte_reposicion_id", using: :btree
    t.index ["tipo_corte_reposicion_id"], name: "index_orden_corte_reposicions_on_tipo_corte_reposicion_id", using: :btree
  end

  create_table "orden_gas_inicials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.integer  "secuencia"
    t.string   "direccion"
    t.string   "calle"
    t.integer  "numero"
    t.datetime "fecha_carga"
    t.datetime "fecha_propuesta"
    t.datetime "fecha_asignacion"
    t.decimal  "gps_latitud",            precision: 10, scale: 7
    t.decimal  "gps_longitud",           precision: 10, scale: 7
    t.boolean  "verificacion"
    t.integer  "modulo_analisis"
    t.boolean  "aprobado"
    t.string   "observacion_analista"
    t.integer  "color"
    t.string   "lectura_encontrada"
    t.string   "observacion_tecnico"
    t.datetime "fecha_ejecucion"
    t.integer  "medidor_id"
    t.integer  "instalacion_id"
    t.integer  "cliente_id"
    t.integer  "ruta_gas_inicial_id"
    t.integer  "proceso_gas_inicial_id"
    t.integer  "estado_gas_inicial_id"
    t.integer  "comuna_id"
    t.integer  "clave_gas_inicial_id"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.index ["clave_gas_inicial_id"], name: "index_orden_gas_inicials_on_clave_gas_inicial_id", using: :btree
    t.index ["cliente_id"], name: "index_orden_gas_inicials_on_cliente_id", using: :btree
    t.index ["comuna_id"], name: "index_orden_gas_inicials_on_comuna_id", using: :btree
    t.index ["estado_gas_inicial_id"], name: "index_orden_gas_inicials_on_estado_gas_inicial_id", using: :btree
    t.index ["instalacion_id"], name: "index_orden_gas_inicials_on_instalacion_id", using: :btree
    t.index ["medidor_id"], name: "index_orden_gas_inicials_on_medidor_id", using: :btree
    t.index ["proceso_gas_inicial_id"], name: "index_orden_gas_inicials_on_proceso_gas_inicial_id", using: :btree
    t.index ["ruta_gas_inicial_id"], name: "index_orden_gas_inicials_on_ruta_gas_inicial_id", using: :btree
  end

  create_table "orden_lecturas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.integer  "secuencia_lector"
    t.string   "direccion"
    t.string   "direccion_entrega"
    t.string   "numero_poste"
    t.string   "observacion_lector"
    t.datetime "fecha_carga"
    t.datetime "fecha_propuesta"
    t.datetime "fecha_asignacion"
    t.decimal  "gps_latitud",              precision: 10, scale: 7
    t.decimal  "gps_longitud",             precision: 10, scale: 7
    t.decimal  "ajuste_sencillo_anterior", precision: 6,  scale: 2
    t.decimal  "ajuste_sencillo_actual",   precision: 6,  scale: 2
    t.boolean  "verificacion"
    t.integer  "modulo_analisis"
    t.boolean  "aprobado"
    t.string   "observacion_analista"
    t.integer  "color"
    t.integer  "medidor_id"
    t.integer  "instalacion_id"
    t.integer  "cliente_id"
    t.integer  "rutum_id"
    t.integer  "tipo_lectura_id"
    t.integer  "estado_lectura_id"
    t.integer  "tarifa_id"
    t.integer  "comuna_id"
    t.integer  "contratista_id"
    t.integer  "carga_sistema_id"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.decimal  "lectura_anterior",         precision: 10
    t.decimal  "lectura_actual",           precision: 10
    t.decimal  "lectura_modificada",       precision: 10
    t.decimal  "lectura_dictada",          precision: 10
    t.datetime "fecha_ejecucion"
    t.decimal  "consumo_minimo",           precision: 10
    t.decimal  "consumo_maximo",           precision: 10
    t.decimal  "consumo_promedio",         precision: 10
    t.decimal  "consumo_actual",           precision: 10
    t.integer  "clave_lectura_id"
    t.string   "numero_medidor"
    t.index ["carga_sistema_id"], name: "index_orden_lecturas_on_carga_sistema_id", using: :btree
    t.index ["clave_lectura_id"], name: "index_orden_lecturas_on_clave_lectura_id", using: :btree
    t.index ["cliente_id"], name: "index_orden_lecturas_on_cliente_id", using: :btree
    t.index ["codigo"], name: "index_orden_lecturas_on_codigo", using: :btree
    t.index ["comuna_id"], name: "index_orden_lecturas_on_comuna_id", using: :btree
    t.index ["contratista_id"], name: "index_orden_lecturas_on_contratista_id", using: :btree
    t.index ["estado_lectura_id"], name: "index_orden_lecturas_on_estado_lectura_id", using: :btree
    t.index ["instalacion_id"], name: "index_orden_lecturas_on_instalacion_id", using: :btree
    t.index ["medidor_id"], name: "index_orden_lecturas_on_medidor_id", using: :btree
    t.index ["rutum_id"], name: "index_orden_lecturas_on_rutum_id", using: :btree
    t.index ["tarifa_id"], name: "index_orden_lecturas_on_tarifa_id", using: :btree
    t.index ["tipo_lectura_id"], name: "index_orden_lecturas_on_tipo_lectura_id", using: :btree
  end

  create_table "orden_repartos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "domicilio"
    t.datetime "fecha_carga"
    t.datetime "fecha_asignacion"
    t.datetime "fecha_ejecucion"
    t.decimal  "gps_latitud",       precision: 10, scale: 7
    t.decimal  "gps_longitud",      precision: 10, scale: 7
    t.integer  "estado_reparto_id"
    t.integer  "tipo_documento_id"
    t.integer  "cliente_id"
    t.integer  "medidor_id"
    t.integer  "instalacion_id"
    t.integer  "secuencia_lector"
    t.string   "direccion_entrega"
    t.integer  "ruta_reparto_id"
    t.integer  "comuna_id"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "numero_cliente"
    t.index ["cliente_id"], name: "index_orden_repartos_on_cliente_id", using: :btree
    t.index ["comuna_id"], name: "index_orden_repartos_on_comuna_id", using: :btree
    t.index ["estado_reparto_id"], name: "index_orden_repartos_on_estado_reparto_id", using: :btree
    t.index ["instalacion_id"], name: "index_orden_repartos_on_instalacion_id", using: :btree
    t.index ["medidor_id"], name: "index_orden_repartos_on_medidor_id", using: :btree
    t.index ["ruta_reparto_id"], name: "index_orden_repartos_on_ruta_reparto_id", using: :btree
    t.index ["tipo_documento_id"], name: "index_orden_repartos_on_tipo_documento_id", using: :btree
  end

  create_table "perfils", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.string   "imagen"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "porcion_cambio_medidors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "zona_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["zona_id"], name: "index_porcion_cambio_medidors_on_zona_id", using: :btree
  end

  create_table "porcion_convenios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "zona_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["zona_id"], name: "index_porcion_convenios_on_zona_id", using: :btree
  end

  create_table "porcion_corte_reposicions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "zona_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["zona_id"], name: "index_porcion_corte_reposicions_on_zona_id", using: :btree
  end

  create_table "porcion_gas_inicials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "zona_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["zona_id"], name: "index_porcion_gas_inicials_on_zona_id", using: :btree
  end

  create_table "porcion_repartos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "zona_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["zona_id"], name: "index_porcion_repartos_on_zona_id", using: :btree
  end

  create_table "porcions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "zona_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["codigo"], name: "index_porcions_on_codigo", using: :btree
    t.index ["nombre"], name: "index_porcions_on_nombre", using: :btree
    t.index ["zona_id"], name: "index_porcions_on_zona_id", using: :btree
  end

  create_table "proceso_cambio_medidors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "proceso_convenios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "proceso_corte_reposicions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "proceso_gas_inicials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "proceso_lecturas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "provincia", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "region_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["region_id"], name: "index_provincia_on_region_id", using: :btree
  end

  create_table "regions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ruta", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "porcion_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "cantidad_lecturas"
    t.index ["codigo"], name: "index_ruta_on_codigo", using: :btree
    t.index ["nombre"], name: "index_ruta_on_nombre", using: :btree
    t.index ["porcion_id"], name: "index_ruta_on_porcion_id", using: :btree
  end

  create_table "ruta_cambio_medidors", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "porcion_cambio_medidor_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["porcion_cambio_medidor_id"], name: "index_ruta_cambio_medidors_on_porcion_cambio_medidor_id", using: :btree
  end

  create_table "ruta_convenios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "porcion_convenio_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["porcion_convenio_id"], name: "index_ruta_convenios_on_porcion_convenio_id", using: :btree
  end

  create_table "ruta_corte_reposicions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "porcion_corte_reposicion_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["porcion_corte_reposicion_id"], name: "index_ruta_corte_reposicions_on_porcion_corte_reposicion_id", using: :btree
  end

  create_table "ruta_gas_inicials", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "porcion_gas_inicial_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["porcion_gas_inicial_id"], name: "index_ruta_gas_inicials_on_porcion_gas_inicial_id", using: :btree
  end

  create_table "ruta_repartos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.integer  "mes"
    t.integer  "ano"
    t.boolean  "abierto"
    t.integer  "porcion_reparto_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "nombre"
    t.index ["porcion_reparto_id"], name: "index_ruta_repartos_on_porcion_reparto_id", using: :btree
  end

  create_table "tarifas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer  "comunas_id"
    t.decimal  "valor_m3",   precision: 10
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.index ["comunas_id"], name: "index_tarifas_on_comunas_id", using: :btree
  end

  create_table "tipo_convenios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "rango_a"
    t.integer  "rango_b"
  end

  create_table "tipo_corte_reposicions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_documentos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_lecturas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "nombre"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_tarjeta", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "descripcion"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "usuarios", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "email",                             default: "", null: false
    t.string   "encrypted_password",                default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                     default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.integer  "perfil_id"
    t.string   "authentication_token",   limit: 30
    t.index ["authentication_token"], name: "index_usuarios_on_authentication_token", unique: true, using: :btree
    t.index ["email"], name: "index_usuarios_on_email", unique: true, using: :btree
    t.index ["perfil_id"], name: "index_usuarios_on_perfil_id", using: :btree
    t.index ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true, using: :btree
  end

  create_table "zonas", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.integer  "region_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["region_id"], name: "index_zonas_on_region_id", using: :btree
  end

  add_foreign_key "asignacion_cambio_medidors", "empleados"
  add_foreign_key "asignacion_cambio_medidors", "orden_cambio_medidors"
  add_foreign_key "asignacion_cambio_medidors", "ruta_cambio_medidors"
  add_foreign_key "asignacion_convenios", "empleados"
  add_foreign_key "asignacion_convenios", "orden_convenios"
  add_foreign_key "asignacion_convenios", "ruta_convenios"
  add_foreign_key "asignacion_corte_reposicions", "empleados"
  add_foreign_key "asignacion_corte_reposicions", "orden_corte_reposicions"
  add_foreign_key "asignacion_corte_reposicions", "ruta_corte_reposicions"
  add_foreign_key "asignacion_gas_inicials", "empleados"
  add_foreign_key "asignacion_gas_inicials", "orden_gas_inicials"
  add_foreign_key "asignacion_gas_inicials", "ruta_gas_inicials"
  add_foreign_key "asignacion_lecturas", "empleados"
  add_foreign_key "asignacion_lecturas", "orden_lecturas"
  add_foreign_key "asignacion_lecturas", "ruta"
  add_foreign_key "asignacion_modificacion_lecturas", "clave_lecturas"
  add_foreign_key "asignacion_modificacion_lecturas", "empleados"
  add_foreign_key "asignacion_modificacion_lecturas", "orden_lecturas"
  add_foreign_key "asignacion_modificacion_lecturas", "ruta"
  add_foreign_key "asignacion_repartos", "empleados"
  add_foreign_key "asignacion_repartos", "orden_repartos"
  add_foreign_key "asignacion_repartos", "ruta_repartos"
  add_foreign_key "clientes", "empresas"
  add_foreign_key "comunas", "provincia"
  add_foreign_key "comunas", "zonas"
  add_foreign_key "detalle_orden_lecturas", "clave_lecturas"
  add_foreign_key "detalle_orden_lecturas", "orden_lecturas"
  add_foreign_key "empleados", "contratista", column: "contratista_id"
  add_foreign_key "empleados", "empresas"
  add_foreign_key "empleados", "usuarios"
  add_foreign_key "empleados_zonas", "empleados"
  add_foreign_key "empleados_zonas", "zonas"
  add_foreign_key "empresas", "comunas"
  add_foreign_key "equipos", "empleados"
  add_foreign_key "equipos", "modelos"
  add_foreign_key "fotografia", "detalle_orden_lecturas"
  add_foreign_key "fotografia", "orden_lecturas"
  add_foreign_key "fotografia_cambio_medidors", "orden_cambio_medidors"
  add_foreign_key "fotografia_convenios", "orden_convenios"
  add_foreign_key "fotografia_corte_reposicions", "orden_corte_reposicions"
  add_foreign_key "fotografia_gas_inicials", "orden_gas_inicials"
  add_foreign_key "intentos", "detalle_orden_lecturas"
  add_foreign_key "medidors", "modelo_medidors"
  add_foreign_key "modelo_medidors", "marca_medidors"
  add_foreign_key "modelos", "marcas"
  add_foreign_key "monitor_lectores", "empleados"
  add_foreign_key "monitor_lectores", "ruta"
  add_foreign_key "orden_cambio_medidors", "clave_cambio_medidores"
  add_foreign_key "orden_cambio_medidors", "clientes"
  add_foreign_key "orden_cambio_medidors", "comunas"
  add_foreign_key "orden_cambio_medidors", "estado_cambio_medidors"
  add_foreign_key "orden_cambio_medidors", "instalacions"
  add_foreign_key "orden_cambio_medidors", "medidors"
  add_foreign_key "orden_cambio_medidors", "proceso_cambio_medidors"
  add_foreign_key "orden_cambio_medidors", "ruta_cambio_medidors"
  add_foreign_key "orden_convenios", "clave_convenios"
  add_foreign_key "orden_convenios", "clientes"
  add_foreign_key "orden_convenios", "comunas"
  add_foreign_key "orden_convenios", "estado_convenios"
  add_foreign_key "orden_convenios", "instalacions"
  add_foreign_key "orden_convenios", "medidors"
  add_foreign_key "orden_convenios", "proceso_convenios"
  add_foreign_key "orden_convenios", "ruta_convenios"
  add_foreign_key "orden_convenios", "tipo_convenios"
  add_foreign_key "orden_convenios", "tipo_tarjeta", column: "tipo_tarjeta_id"
  add_foreign_key "orden_corte_reposicions", "clave_corte_reposicions"
  add_foreign_key "orden_corte_reposicions", "clientes"
  add_foreign_key "orden_corte_reposicions", "comunas"
  add_foreign_key "orden_corte_reposicions", "estado_corte_reposicions"
  add_foreign_key "orden_corte_reposicions", "instalacions"
  add_foreign_key "orden_corte_reposicions", "medidors"
  add_foreign_key "orden_corte_reposicions", "proceso_corte_reposicions"
  add_foreign_key "orden_corte_reposicions", "ruta_corte_reposicions"
  add_foreign_key "orden_corte_reposicions", "tipo_corte_reposicions"
  add_foreign_key "orden_gas_inicials", "clave_gas_inicials"
  add_foreign_key "orden_gas_inicials", "clientes"
  add_foreign_key "orden_gas_inicials", "comunas"
  add_foreign_key "orden_gas_inicials", "estado_gas_inicials"
  add_foreign_key "orden_gas_inicials", "instalacions"
  add_foreign_key "orden_gas_inicials", "medidors"
  add_foreign_key "orden_gas_inicials", "proceso_gas_inicials"
  add_foreign_key "orden_gas_inicials", "ruta_gas_inicials"
  add_foreign_key "orden_lecturas", "carga_sistemas"
  add_foreign_key "orden_lecturas", "clave_lecturas"
  add_foreign_key "orden_lecturas", "clientes"
  add_foreign_key "orden_lecturas", "comunas"
  add_foreign_key "orden_lecturas", "contratista", column: "contratista_id"
  add_foreign_key "orden_lecturas", "estado_lecturas"
  add_foreign_key "orden_lecturas", "instalacions"
  add_foreign_key "orden_lecturas", "medidors"
  add_foreign_key "orden_lecturas", "ruta"
  add_foreign_key "orden_lecturas", "tarifas"
  add_foreign_key "orden_lecturas", "tipo_lecturas"
  add_foreign_key "orden_repartos", "clientes"
  add_foreign_key "orden_repartos", "comunas"
  add_foreign_key "orden_repartos", "estado_repartos"
  add_foreign_key "orden_repartos", "instalacions"
  add_foreign_key "orden_repartos", "medidors"
  add_foreign_key "orden_repartos", "ruta_repartos"
  add_foreign_key "orden_repartos", "tipo_documentos"
  add_foreign_key "porcion_cambio_medidors", "zonas"
  add_foreign_key "porcion_convenios", "zonas"
  add_foreign_key "porcion_corte_reposicions", "zonas"
  add_foreign_key "porcion_gas_inicials", "zonas"
  add_foreign_key "porcion_repartos", "zonas"
  add_foreign_key "porcions", "zonas"
  add_foreign_key "provincia", "regions"
  add_foreign_key "ruta", "porcions"
  add_foreign_key "ruta_cambio_medidors", "porcion_cambio_medidors"
  add_foreign_key "ruta_convenios", "porcion_convenios"
  add_foreign_key "ruta_corte_reposicions", "porcion_corte_reposicions"
  add_foreign_key "ruta_gas_inicials", "porcion_gas_inicials"
  add_foreign_key "ruta_repartos", "porcion_repartos"
  add_foreign_key "tarifas", "comunas", column: "comunas_id"
  add_foreign_key "usuarios", "perfils"
  add_foreign_key "zonas", "regions"
end
