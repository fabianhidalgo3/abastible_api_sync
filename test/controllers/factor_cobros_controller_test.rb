require 'test_helper'

class FactorCobrosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @factor_cobro = factor_cobros(:one)
  end

  test "should get index" do
    get factor_cobros_url
    assert_response :success
  end

  test "should get new" do
    get new_factor_cobro_url
    assert_response :success
  end

  test "should create factor_cobro" do
    assert_difference('FactorCobro.count') do
      post factor_cobros_url, params: { factor_cobro: { cargo_energia_adicional: @factor_cobro.cargo_energia_adicional, cargo_energia_base: @factor_cobro.cargo_energia_base, cargo_fijo: @factor_cobro.cargo_fijo, cargo_unico: @factor_cobro.cargo_unico, precio_energia_inyectada: @factor_cobro.precio_energia_inyectada, sector_id: @factor_cobro.sector_id, subempresa_id: @factor_cobro.subempresa_id, tipo_sector_id: @factor_cobro.tipo_sector_id, tipo_tarifa_id: @factor_cobro.tipo_tarifa_id } }
    end

    assert_redirected_to factor_cobro_url(FactorCobro.last)
  end

  test "should show factor_cobro" do
    get factor_cobro_url(@factor_cobro)
    assert_response :success
  end

  test "should get edit" do
    get edit_factor_cobro_url(@factor_cobro)
    assert_response :success
  end

  test "should update factor_cobro" do
    patch factor_cobro_url(@factor_cobro), params: { factor_cobro: { cargo_energia_adicional: @factor_cobro.cargo_energia_adicional, cargo_energia_base: @factor_cobro.cargo_energia_base, cargo_fijo: @factor_cobro.cargo_fijo, cargo_unico: @factor_cobro.cargo_unico, precio_energia_inyectada: @factor_cobro.precio_energia_inyectada, sector_id: @factor_cobro.sector_id, subempresa_id: @factor_cobro.subempresa_id, tipo_sector_id: @factor_cobro.tipo_sector_id, tipo_tarifa_id: @factor_cobro.tipo_tarifa_id } }
    assert_redirected_to factor_cobro_url(@factor_cobro)
  end

  test "should destroy factor_cobro" do
    assert_difference('FactorCobro.count', -1) do
      delete factor_cobro_url(@factor_cobro)
    end

    assert_redirected_to factor_cobros_url
  end
end
