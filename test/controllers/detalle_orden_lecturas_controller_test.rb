require 'test_helper'

class DetalleOrdenLecturasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @detalle_orden_lectura = detalle_orden_lecturas(:one)
  end

  test "should get index" do
    get detalle_orden_lecturas_url
    assert_response :success
  end

  test "should get new" do
    get new_detalle_orden_lectura_url
    assert_response :success
  end

  test "should create detalle_orden_lectura" do
    assert_difference('DetalleOrdenLectura.count') do
      post detalle_orden_lecturas_url, params: { detalle_orden_lectura: { clave_lectura_id: @detalle_orden_lectura.clave_lectura_id, fecha_ejecucion: @detalle_orden_lectura.fecha_ejecucion, lectura_actual: @detalle_orden_lectura.lectura_actual, lectura_anterior: @detalle_orden_lectura.lectura_anterior, lectura_promedio: @detalle_orden_lectura.lectura_promedio, numerador_id: @detalle_orden_lectura.numerador_id, observacion_id: @detalle_orden_lectura.observacion_id, orden_lectura_id: @detalle_orden_lectura.orden_lectura_id, rango_inferior: @detalle_orden_lectura.rango_inferior, rango_superior: @detalle_orden_lectura.rango_superior } }
    end

    assert_redirected_to detalle_orden_lectura_url(DetalleOrdenLectura.last)
  end

  test "should show detalle_orden_lectura" do
    get detalle_orden_lectura_url(@detalle_orden_lectura)
    assert_response :success
  end

  test "should get edit" do
    get edit_detalle_orden_lectura_url(@detalle_orden_lectura)
    assert_response :success
  end

  test "should update detalle_orden_lectura" do
    patch detalle_orden_lectura_url(@detalle_orden_lectura), params: { detalle_orden_lectura: { clave_lectura_id: @detalle_orden_lectura.clave_lectura_id, fecha_ejecucion: @detalle_orden_lectura.fecha_ejecucion, lectura_actual: @detalle_orden_lectura.lectura_actual, lectura_anterior: @detalle_orden_lectura.lectura_anterior, lectura_promedio: @detalle_orden_lectura.lectura_promedio, numerador_id: @detalle_orden_lectura.numerador_id, observacion_id: @detalle_orden_lectura.observacion_id, orden_lectura_id: @detalle_orden_lectura.orden_lectura_id, rango_inferior: @detalle_orden_lectura.rango_inferior, rango_superior: @detalle_orden_lectura.rango_superior } }
    assert_redirected_to detalle_orden_lectura_url(@detalle_orden_lectura)
  end

  test "should destroy detalle_orden_lectura" do
    assert_difference('DetalleOrdenLectura.count', -1) do
      delete detalle_orden_lectura_url(@detalle_orden_lectura)
    end

    assert_redirected_to detalle_orden_lecturas_url
  end
end
