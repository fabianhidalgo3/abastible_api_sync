require 'test_helper'

class FotografiaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fotografium = fotografia(:one)
  end

  test "should get index" do
    get fotografia_url
    assert_response :success
  end

  test "should get new" do
    get new_fotografium_url
    assert_response :success
  end

  test "should create fotografium" do
    assert_difference('Fotografium.count') do
      post fotografia_url, params: { fotografium: { archivo: @fotografium.archivo, descripcion: @fotografium.descripcion, detalle_orden_lectura_id: @fotografium.detalle_orden_lectura_id } }
    end

    assert_redirected_to fotografium_url(Fotografium.last)
  end

  test "should show fotografium" do
    get fotografium_url(@fotografium)
    assert_response :success
  end

  test "should get edit" do
    get edit_fotografium_url(@fotografium)
    assert_response :success
  end

  test "should update fotografium" do
    patch fotografium_url(@fotografium), params: { fotografium: { archivo: @fotografium.archivo, descripcion: @fotografium.descripcion, detalle_orden_lectura_id: @fotografium.detalle_orden_lectura_id } }
    assert_redirected_to fotografium_url(@fotografium)
  end

  test "should destroy fotografium" do
    assert_difference('Fotografium.count', -1) do
      delete fotografium_url(@fotografium)
    end

    assert_redirected_to fotografia_url
  end
end
