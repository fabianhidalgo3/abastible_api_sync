require 'test_helper'

class ContratistaSubempresasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contratista_subempresa = contratista_subempresas(:one)
  end

  test "should get index" do
    get contratista_subempresas_url
    assert_response :success
  end

  test "should get new" do
    get new_contratista_subempresa_url
    assert_response :success
  end

  test "should create contratista_subempresa" do
    assert_difference('ContratistaSubempresa.count') do
      post contratista_subempresas_url, params: { contratista_subempresa: { contratistum_id: @contratista_subempresa.contratistum_id, subempresa_id: @contratista_subempresa.subempresa_id } }
    end

    assert_redirected_to contratista_subempresa_url(ContratistaSubempresa.last)
  end

  test "should show contratista_subempresa" do
    get contratista_subempresa_url(@contratista_subempresa)
    assert_response :success
  end

  test "should get edit" do
    get edit_contratista_subempresa_url(@contratista_subempresa)
    assert_response :success
  end

  test "should update contratista_subempresa" do
    patch contratista_subempresa_url(@contratista_subempresa), params: { contratista_subempresa: { contratistum_id: @contratista_subempresa.contratistum_id, subempresa_id: @contratista_subempresa.subempresa_id } }
    assert_redirected_to contratista_subempresa_url(@contratista_subempresa)
  end

  test "should destroy contratista_subempresa" do
    assert_difference('ContratistaSubempresa.count', -1) do
      delete contratista_subempresa_url(@contratista_subempresa)
    end

    assert_redirected_to contratista_subempresas_url
  end
end
