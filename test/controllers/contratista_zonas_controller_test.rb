require 'test_helper'

class ContratistaZonasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contratista_zona = contratista_zonas(:one)
  end

  test "should get index" do
    get contratista_zonas_url
    assert_response :success
  end

  test "should get new" do
    get new_contratista_zona_url
    assert_response :success
  end

  test "should create contratista_zona" do
    assert_difference('ContratistaZona.count') do
      post contratista_zonas_url, params: { contratista_zona: { contratista_id: @contratista_zona.contratista_id, zona_id: @contratista_zona.zona_id } }
    end

    assert_redirected_to contratista_zona_url(ContratistaZona.last)
  end

  test "should show contratista_zona" do
    get contratista_zona_url(@contratista_zona)
    assert_response :success
  end

  test "should get edit" do
    get edit_contratista_zona_url(@contratista_zona)
    assert_response :success
  end

  test "should update contratista_zona" do
    patch contratista_zona_url(@contratista_zona), params: { contratista_zona: { contratista_id: @contratista_zona.contratista_id, zona_id: @contratista_zona.zona_id } }
    assert_redirected_to contratista_zona_url(@contratista_zona)
  end

  test "should destroy contratista_zona" do
    assert_difference('ContratistaZona.count', -1) do
      delete contratista_zona_url(@contratista_zona)
    end

    assert_redirected_to contratista_zonas_url
  end
end
