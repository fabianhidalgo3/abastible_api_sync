require 'test_helper'

class ClaveFotografiaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @clave_fotografium = clave_fotografia(:one)
  end

  test "should get index" do
    get clave_fotografia_url
    assert_response :success
  end

  test "should get new" do
    get new_clave_fotografium_url
    assert_response :success
  end

  test "should create clave_fotografium" do
    assert_difference('ClaveFotografium.count') do
      post clave_fotografia_url, params: { clave_fotografium: { nombre: @clave_fotografium.nombre } }
    end

    assert_redirected_to clave_fotografium_url(ClaveFotografium.last)
  end

  test "should show clave_fotografium" do
    get clave_fotografium_url(@clave_fotografium)
    assert_response :success
  end

  test "should get edit" do
    get edit_clave_fotografium_url(@clave_fotografium)
    assert_response :success
  end

  test "should update clave_fotografium" do
    patch clave_fotografium_url(@clave_fotografium), params: { clave_fotografium: { nombre: @clave_fotografium.nombre } }
    assert_redirected_to clave_fotografium_url(@clave_fotografium)
  end

  test "should destroy clave_fotografium" do
    assert_difference('ClaveFotografium.count', -1) do
      delete clave_fotografium_url(@clave_fotografium)
    end

    assert_redirected_to clave_fotografia_url
  end
end
