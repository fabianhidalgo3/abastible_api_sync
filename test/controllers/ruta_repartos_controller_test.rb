require 'test_helper'

class RutaRepartosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ruta_reparto = ruta_repartos(:one)
  end

  test "should get index" do
    get ruta_repartos_url
    assert_response :success
  end

  test "should get new" do
    get new_ruta_reparto_url
    assert_response :success
  end

  test "should create ruta_reparto" do
    assert_difference('RutaReparto.count') do
      post ruta_repartos_url, params: { ruta_reparto: { codigo: @ruta_reparto.codigo, nombre: @ruta_reparto.nombre, porcion_reparto_id: @ruta_reparto.porcion_reparto_id } }
    end

    assert_redirected_to ruta_reparto_url(RutaReparto.last)
  end

  test "should show ruta_reparto" do
    get ruta_reparto_url(@ruta_reparto)
    assert_response :success
  end

  test "should get edit" do
    get edit_ruta_reparto_url(@ruta_reparto)
    assert_response :success
  end

  test "should update ruta_reparto" do
    patch ruta_reparto_url(@ruta_reparto), params: { ruta_reparto: { codigo: @ruta_reparto.codigo, nombre: @ruta_reparto.nombre, porcion_reparto_id: @ruta_reparto.porcion_reparto_id } }
    assert_redirected_to ruta_reparto_url(@ruta_reparto)
  end

  test "should destroy ruta_reparto" do
    assert_difference('RutaReparto.count', -1) do
      delete ruta_reparto_url(@ruta_reparto)
    end

    assert_redirected_to ruta_repartos_url
  end
end
