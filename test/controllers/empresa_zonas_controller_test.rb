require 'test_helper'

class EmpresaZonasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @empresa_zona = empresa_zonas(:one)
  end

  test "should get index" do
    get empresa_zonas_url
    assert_response :success
  end

  test "should get new" do
    get new_empresa_zona_url
    assert_response :success
  end

  test "should create empresa_zona" do
    assert_difference('EmpresaZona.count') do
      post empresa_zonas_url, params: { empresa_zona: { empresa_id: @empresa_zona.empresa_id, zona_id: @empresa_zona.zona_id } }
    end

    assert_redirected_to empresa_zona_url(EmpresaZona.last)
  end

  test "should show empresa_zona" do
    get empresa_zona_url(@empresa_zona)
    assert_response :success
  end

  test "should get edit" do
    get edit_empresa_zona_url(@empresa_zona)
    assert_response :success
  end

  test "should update empresa_zona" do
    patch empresa_zona_url(@empresa_zona), params: { empresa_zona: { empresa_id: @empresa_zona.empresa_id, zona_id: @empresa_zona.zona_id } }
    assert_redirected_to empresa_zona_url(@empresa_zona)
  end

  test "should destroy empresa_zona" do
    assert_difference('EmpresaZona.count', -1) do
      delete empresa_zona_url(@empresa_zona)
    end

    assert_redirected_to empresa_zonas_url
  end
end
