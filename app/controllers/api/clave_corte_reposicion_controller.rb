# class Api::ClaveCorteReposicionController < Api::ApplicationController
#   before_action :set_clave_corte_reposicion, only: [:show, :edit, :update, :destroy]
  

#   # GET /clave_corte_reposicions
#   # GET /clave_corte_reposicions.json
#   def index
#     @clave_corte_reposicions = ClaveCorteReposicion.all
#     respond_to do |format|
#       format.json { render :json => @clave_corte_reposicions }
#     end
#   end

#   # GET /clave_corte_reposicions/1
#   # GET /clave_corte_reposicions/1.json
#   def show
#   end

#   # GET /clave_corte_reposicions/new
#   def new
#     @clave_corte_reposicion = ClaveCorteReposicion.new
#   end

#   # GET /clave_corte_reposicions/1/edit
#   def edit
#   end

#   # POST /clave_corte_reposicions
#   # POST /clave_corte_reposicions.
#   def create
#     @clave_corte_reposicion = ClaveCorteReposicion.new(clave_corte_reposicion_params)
#     respond_to do |format|
#       if @clave_corte_reposicion.save
#         format.html { redirect_to @clave_corte_reposicion, notice: 'Clave Corte Reposicion was successfully created.' }
#         format.json { render :show, status: :created, location: @clave_corte_reposicion }
#       else
#         format.html { render :new }
#         format.json { render json: @clave_corte_reposicion.errors, status: :unprocessable_entity }
#       end
#     end
#   end

#   # PATCH/PUT /clave_corte_reposicions/1
#   # PATCH/PUT /clave_corte_reposicions/1.json
#   def update
#     respond_to do |format|
#       if @clave_corte_reposicion.update(clave_corte_reposicion_params)
#         format.html { redirect_to @clave_corte_reposicion, notice: 'Clave lectura was successfully updated.' }
#         format.json { render :show, status: :ok, location: @clave_corte_reposicion }
#       else
#         format.html { render :edit }
#         format.json { render json: @clave_corte_reposicion.errors, status: :unprocessable_entity }
#       end
#     end
#   end

#   # DELETE /clave_corte_reposicions/1
#   # DELETE /clave_corte_reposicions/1.json
#   def destroy
#     @clave_corte_reposicion.destroy
#     respond_to do |format|
#       format.html { redirect_to clave_corte_reposicions_url, notice: 'Clave lectura was successfully destroyed.' }
#       format.json { head :no_content }
#     end
#   end

#   private
#     # Use callbacks to share common setup or constraints between actions.
#     def set_clave_corte_reposicion
#       @clave_corte_reposicion = ClaveLectura.find(params[:id])
#     end

#     # Never trust parameters from the scary internet, only allow the white list through.
#     def clave_corte_reposicion_params
#       params.require(:clave_corte_reposicion).permit(:nombre, :tipo_cobro_id)
#     end
# end
