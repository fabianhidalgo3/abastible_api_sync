class Api::AsignacionsController < Api::ApplicationController
  before_action :set_asignacion, only: [:show, :edit, :update, :destroy]
  # GET /asignacions
  # GET /asignacions.json
  
  def index
    @asignacions = AsignacionLectura.joins(:empleado, :orden_lectura).where(empleados:{usuario_id: params[:id]}, orden_lecturas:{estado_lectura_id:[2..3]})    
    respond_to do |format|
      format.json { render :json => @asignacions }
    end
  end

  def desasignacion
    @asignacions = AsignacionLectura.joins(:empleado, :orden_lectura).where(empleados:{usuario_id: params[:id]}, orden_lecturas:{id: params[:orden_id]}).first
    if @asignacions.nil?
      @asignacions = AsignacionLectura.new
    end

    respond_to do |format|
      format.json { render :json => @asignacions }
    end
  end
end
