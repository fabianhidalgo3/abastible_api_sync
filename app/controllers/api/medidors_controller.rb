class Api::MedidorsController < Api::ApplicationController
  before_action :set_medidor, only: [:show, :edit, :update, :destroy]

  # GET /medidors
  # GET /medidors.json
  def index
    @medidor = Medidor.find(params[:id])
    respond_to do |format|
      format.json { render :json => @medidor }
    end
  end
end
