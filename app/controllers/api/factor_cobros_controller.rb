class Api::FactorCobrosController < Api::ApplicationController

# GET /equipos
# GET /equipos.json
  def index
    @factorCobro = FactorCobro.all
    respond_to do |format|
      format.json { render :json => @factorCobro }
    end
  end
end