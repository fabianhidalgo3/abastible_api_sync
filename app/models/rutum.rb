class Rutum < ApplicationRecord
  belongs_to :porcion
  has_many :orden_lectura
  has_many :asignacion_lectura

  def nombre
    if self[:nombre].nil?
      ""
    else
      self[:nombre]
    end
  end

end
