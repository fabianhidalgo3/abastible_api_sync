class OrdenReparto < ApplicationRecord
  belongs_to :estado_reparto
  belongs_to :tipo_reparto
  belongs_to :cliente
  belongs_to :medidor
  belongs_to :instalacion
  belongs_to :ruta_reparto
  belongs_to :comuna
  has_one :asignacion_reparto
end
