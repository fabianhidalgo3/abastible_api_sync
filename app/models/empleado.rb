class Empleado < ApplicationRecord
  belongs_to :contratistum
  belongs_to :empresa
  has_many  :orden_lectura
  belongs_to  :tipo_tarifa
  belongs_to  :sector
  belongs_to  :tipo_sector
	has_many :asignacion_lectura
	has_many :asignacion_reparto
  has_and_belongs_to_many :zona
  has_one :equipo
  belongs_to :usuario

	validates :rut, uniqueness: { message: "Rut ya registrado."}

  def nombre_completo
    # Valido que exista el nombre
    if nombre.nil?
      @nombre_completo = apellido_paterno + " " + apellido_materno
    end
    # Valido que exitan los apellidos
    if apellido_paterno.nil? && apellido_materno.nil?
      @nombre_completo = nombre
    else 
      @nombre_completo = nombre + " " + apellido_paterno + " " + apellido_materno
    end
  end
end
