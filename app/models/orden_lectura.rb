class OrdenLectura < ApplicationRecord
  belongs_to :instalacion
  belongs_to :cliente
  belongs_to :contratistum
  belongs_to :rutum
  belongs_to :tipo_lectura
  belongs_to :estado_lectura
  belongs_to :tarifa
  belongs_to :porcion
  belongs_to :comuna
  belongs_to :empleado
  has_one :asignacion_lectura
  has_many :detalle_orden_lectura
  belongs_to :clave_lectura
  belongs_to :medidor
  has_many :fotografium
  #enum modulo_analisis: [consumo_cero:0, consumo_inferior:2, consumo_maximo:3, improcedencia:4]

  def direccion_completa
    @direccion_orden = direccion.to_s + " " + block.to_s + " " + numero_propieda.to_s + " " + numero_departamento.to_s
  end
end
