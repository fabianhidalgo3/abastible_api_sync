class Tarifa < ApplicationRecord
  belongs_to :comuna
  has_many :orden_lectura
  has_and_belongs_to_many :comuna
end
