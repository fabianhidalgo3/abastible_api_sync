class Fotografium < ApplicationRecord
  belongs_to :detalle_orden_lectura
  belongs_to :orden_lectura
  validates :archivo, uniqueness: true
end
